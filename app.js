//run with --inspect to debug in chrome web browser

var express = require('express'),
	http = require('http'),
	https = require('https'),
	fs = require('fs'),
	bodyParser = require('body-parser'),
	multer = require('multer');


var app = express();
var privateKey = fs.readFileSync('assets/selfsigned.key', 'utf8');
var certificate = fs.readFileSync('assets/selfsigned.crt', 'utf8');
var credentials = {key: privateKey, cert: certificate};


const upload = multer({dest:'uploads/', onError: function(err, nxt){
	log('error when trying to upload file');
	nxt(err);
}});


var file = upload.single('file');
var mfile = upload.array('file', 3);

app.use(express.json());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));
//app.use(express.static('public')) //become available to browse

app.get('/status', function(req, res){
	var status = new String(req.query.s);

	if (!status)
		res.sendStatus(500);
	else
		res.sendStatus(parseInt(status));
});

app.get('/', function(req, res){
	res.send('Hello World');
});

app.get('/first', function(req, res){
	res.send('Hello First');
	res.append('Hello');

});

app.get('/second/third', function(req, res){
	res.send('Hello Third');
});

app.get('/route/with/many/paths', function(req, res){
	res.send('Congrats you\'ve reached the route with many paths');
})

//this method will take the body received and sent it back to you
app.post('/post', function(req,res){

	if(req.body == '' || req.body == {})
		log('Nothing came to us. Try send it again with some content.');
	else
		log("We've received " + req.body);

	res.json(req.body);
});

function log(message){
	console.log(new Date() + ' - ' + message);
}

app.post('/upload/multiple', (req, res) => {
	mfile(req,res, (err) => {

		if (req.body)
			log('this request contains a body ' + JSON.stringify(req.body));

		if (err){
			log('something doesn\'t work as expected.')
			log(err);
			res.sendStatus(500);
		}
		else{
			log('file uploaded');
			res.send('everything is fine!');
		}
	})
});


//[file] stands for the name of the data on the multipart-form-data content
app.post('/upload', (req,res) => {

	file(req,res, (err) => {
		if (req.body)
			log('this request contains a body ' + JSON.stringify(req.body));


		if (err){
			log('something doesnt worked as expected.');
			log(err);
			res.sendStatus(500);
		}
		else{
			log('file uploaded');
			res.send('everything is fine');
		}

	});



});

app.get('/files', function(req, res){
	var fileName = "mysheet.xlsx"
	const uploadFolder = 'uploads/'

	log('Requesting download of ' + fileName);

	var path = uploadFolder + fileName;

	log('Checking existence of path ' + path);

	if (fs.existsSync(path)){
		log('Yes, we got it');

		var options = {
			root: __dirname + '/' + uploadFolder
		};

		res.sendFile(fileName, options, function(err){
			if (err)
				next(err);
			else
				log('Sent!');
		});
	}
	else {
		log('Sorry pal, file not found.');
		res.sendStatus(404)
	}
});

app.get('/files/download/:file', function(req, res){
	var fileName = req.params.file;
	const uploadFolder = 'uploads/'

	log('Requesting download of ' + fileName);

	var path = uploadFolder + fileName;

	log('Checking existence of path ' + path);

	if (fs.existsSync(path)){
		log('Yes, we got it');

		var options = {
			root: __dirname + '/' + uploadFolder
		};

		res.sendFile(fileName, options, function(err){
			if (err)
				next(err);
			else
				log('Sent!');
		});
	}
	else {
		log('Sorry pal, file not found.');
		res.sendStatus(404)
	}
});

var httpPort = '8080',
	httpsPort = '8443';

var httpsServer = https.createServer(credentials, app);
httpsServer.listen(httpsPort, function(){
	log('HTTPS listening on port: ' + httpsPort.toString());
});

var httpServer = http.createServer(app);
httpServer.listen(httpPort, function(){
	log('HTTP listening on port: ' + httpPort.toString());
});
